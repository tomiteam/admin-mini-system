<?php

namespace App\Http\Controllers;

use App\Moderator;

class ModeratorController extends Controller
{
    /**
     * ModeratorController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $moderators = Moderator::all();
        return view('moderators.index', ['moderators' => $moderators]);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id)
    {
        $moderator = Moderator::find($id);
        return view('moderators.profile', ['moderator' => $moderator]);
    }
}
