<?php

namespace App;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $table = 'articles';

    protected $primaryKey = 'id';

    protected $fillable = [
        'title','body',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function moderator()
    {
        return $this->belongsTo('App\Moderator');
    }

    /**
     * @param $request
     */
    public function create($request)
    {
        $this->moderator_id = Auth::id();
        $this->title = $request->title;
        $this->body = $request->body;
        $this->save();
    }

    /**
     * @param $request
     */
    public function updateArticle($request){
        $this->title = $request->title;
        $this->body = $request->body;
        $this->save();
    }
}
