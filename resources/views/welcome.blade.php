@extends('layouts.app')

@section('title', 'Home')

@section('content')

    <div class="container">
        <div class="center-div">
            All articles:
        </div>
        @if(empty($articles[0]))
            <div class="no-articles">
                There is no articles yet.
            </div>
        @endif
        <ul class="list-group">
            @foreach($articles as $article)
                <li class="list-group-item center-article-info">
                    <div class="title">
                        {{$article->title}}
                    </div>
                    <hr>
                    <div class="body">
                        {{$article->body}}
                    </div>
                    <hr>
                    Written on: {{$article->created_at}}
                    <br>
                    @if(($article->created_at != $article->updated_at))
                        Last updated on: {{$article->updated_at}}
                        <br>
                    @endif
                </li>
            @endforeach
            <div class="pagination">
                {{$articles->links()}}
            </div>
        </ul>
    </div>

@endsection