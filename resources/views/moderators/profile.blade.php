@extends('layouts.app')

@section('title', 'Profile')

@section('content')
    <div class="container">
        <div class="center-div">
            {{$moderator->name}}'s profile
        </div>
        <ul class="list-group">
            <li class="list-group-item">
                ID: {{$moderator->id}}
            </li>
            <li class="list-group-item">
                Username: {{$moderator->username}}
            </li>
            <li class="list-group-item">
                E-mail: {{$moderator->email}}
            </li>
        </ul>
        <div class="center-div">
            {{$moderator->name}}'s articles
        </div>
        <ul class="list-group">
            @if(empty($moderator->articles[0]))
                <div class="no-articles">
                    There is no articles yet.
                </div>
            @endif
            @foreach($moderator->articles as $article)
                <li class="list-group-item list-articles">
                    <div>
                        Title: {{$article->title}}
                    </div>
                    <div>
                        Body: {{$article->body}}
                    </div>
                    <div>
                        <a href="/articles/{{$article->id}}" class="btn btn-info" id="read-article">
                            Read
                        </a>
                    </div>
                      @if((Auth::id() == '1') || (Auth::id() == $article->moderator->id))
                        <form action="/articles/{{$article->id}}" method="post">
                            <input type="hidden" name="_method" value="delete">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="submit" value="Delete" class="btn btn-danger">
                        </form>

                        <form action="/articles/{{$article->id}}/edit" method="get">
                            <input type="submit" value="Update" class="btn btn-success">
                        </form>
                    @endif
                </li>
                @endforeach
        </ul>
    </div>
@endsection