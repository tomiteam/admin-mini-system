@extends('layouts.app')

@section('title', 'Moderators')

@section('content')

    <div class="container">
        <div class="center-div">
            All moderators:
        </div>
        <ul class="list-group">
            @foreach($moderators as $moderator)
                <li class="list-group-item">
                    @if(Auth::id() == '1')
                        <a href="/moderators/show/{{$moderator->id}}">
                            {{$moderator->name}}
                        </a>
                        @else
                        {{$moderator->name}}
                    @endif
                </li>
            @endforeach
        </ul>
    </div>

@endsection