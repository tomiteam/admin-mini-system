@extends('layouts.app')

@section('title', '{{$article->title}}')

@section('content')

    <div class="container">
        <div class="center-div">
            {{$article->title}}
        </div>
        <div>
            {{$article->body}}
        </div>
        <div>
            {{$article->created_at}}
        </div>
    </div>

@endsection