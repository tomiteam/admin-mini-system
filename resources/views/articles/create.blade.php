@extends("layouts.app")

@section('title', 'Create')

@section("content")

    <div class="container">
        <div class="center-div">
            Create article
        </div>

        @if(count($errors) > 0)
            @foreach($errors->all() as $error)
                <div class="error">
                    {{$error}}
                </div>
            @endforeach
        @endif

        <form action="/articles" method="post" class="center-form">

            {{ csrf_field() }}

            <label for="title">Title</label>
            <input type="text" name="title" id="title" placeholder="Title" autofocus>

            <label for="body">Body</label>
            <textarea name="body" id="body"></textarea>

            <input type="submit" id="submit-article" class="btn btn-success" value="Create">
        </form>

    </div>

@endsection