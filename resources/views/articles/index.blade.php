@extends('layouts.app')

@section('title', 'Articles')

@section('content')

    <div class="container">

        <div class="center-div">
            All articles:
        </div>

        @if(empty($articles[0]))
            <div class="no-articles">
                There is no articles yet.
            </div>
        @endif

        <ul class="list-group">
            @foreach($articles as $article)
                <li class="list-group-item">
                    Title: {{$article->title}}
                    <br>
                    Written by: {{$article->moderator->name}}
                    <br>
                    Written on: {{$article->created_at}}
                    <br>
                    @if(($article->created_at != $article->updated_at))
                        Last updated on: {{$article->updated_at}}
                        <br>
                    @endif
                    <a href="/articles/{{$article->id}}" class="btn btn-info" id="read-article">
                        Read
                    </a>
                    @if((Auth::id() == '1') || (Auth::id() == $article->moderator->id))
                        <form action="/articles/{{$article->id}}" method="post">
                            <input type="hidden" name="_method" value="delete">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="submit" value="Delete" class="btn btn-danger">
                        </form>

                        <form action="/articles/{{$article->id}}/edit" method="get">
                            <input type="submit" value="Update" class="btn btn-success">
                        </form>
                    @endif
                </li>
            @endforeach
        </ul>
    </div>

@endsection