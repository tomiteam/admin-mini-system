@extends("layouts.app")

@section('title', 'Edit')

@section("content")

    <div class="container">
        <div class="center-div">
            Update article
        </div>

        @if(count($errors) > 0)
            @foreach($errors->all() as $error)
                <div class="error">
                    {{$error}}
                </div>
            @endforeach
        @endif

        <form action="/articles/{{$article->id}}" method="post" class="center-form">

            <input type="hidden" name="_method" value="put">
            {{ csrf_field() }}

            <label for="title">Title</label>
            <input type="text" name="title" id="title"
                   value="{{$article->title}}" autofocus >

            <label for="body">Body</label>
            <textarea name="body" id="body">{{$article->body}}</textarea>

            <input type="submit" class="btn btn-success" value="Update">
        </form>
    </div>

@endsection